const BaseBotHelper = require('./base.bot.helper');
const DatesBotHelper = require('./dates.bot.helper');
const datesBotHelper = Symbol('datesBotHelper');
const reversedAssociations = Symbol('reversedAssociations');

class PermissionsBotHelper extends BaseBotHelper {
  constructor(slackInstance, revAssociations) {
    super(slackInstance);

    this[datesBotHelper] = new DatesBotHelper(slackInstance);
    this[reversedAssociations] = revAssociations;
  }

  toString(permission) {
    return [
      `> _${this[datesBotHelper].format(permission.createdAt)}_`,
      `*${permission['granter.name']}* предоставил`,
      `пользователю *${permission['user.name']}*`,
      `права на исполнение команды \`${this[reversedAssociations].get(permission.action)}\``
    ].join(' ');
  }
}

module.exports = PermissionsBotHelper;
