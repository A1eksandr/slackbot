const BaseBotHelper = require('./base.bot.helper');
const DatesBotHelper = require('./dates.bot.helper');
const datesBotHelper = Symbol('datesBotHelper');
const statuses = Symbol('statuses');

class TasksBotHelper extends BaseBotHelper {
  constructor(slackInstance, taskStatuses) {
    super(slackInstance);

    this[statuses] = taskStatuses;
    this[datesBotHelper] = new DatesBotHelper(slackInstance);
  }

  toString(task) {
    const message = [
      `${task.id}.`,
      `\`${this[datesBotHelper].format(task.createdAt)}\``,
      `\`${this[statuses].get(task.status)}\``,
      `\`${task.description}\``
    ];

    if (task['user.name']) {
      message.splice(2, 0, `${task['user.name']}`);
    }

    let messageText = message.join(' ');
    switch(task.status) {
      case 'created':
        messageText = `*${messageText}*`;
        break;
      case 'finished':
        messageText = `~${messageText}~`;
        break;
    }
    return messageText;
  }
}

module.exports = TasksBotHelper;
