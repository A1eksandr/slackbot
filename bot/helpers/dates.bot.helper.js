const Intl = require('intl');
const BaseBotHelper = require('./base.bot.helper');
const dateOptions = {
  day: 'numeric',
  year: 'numeric',
  month: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  second: 'numeric',
  hour12: false,
  timeZone: 'UTC',
  timeZoneName: 'short'
};

class DatesBotHelper extends BaseBotHelper {
  constructor(slackInstance) {
    super(slackInstance);
  }

  formatDate(date) {
    if (!date) {
      return;
    }
    return date.toLocaleDateString('ru-RU', dateOptions);
  }

  format(stringDate) {
    if (!stringDate || stringDate.length) {
      return;
    }
    return Intl.DateTimeFormat('ru-RU', dateOptions).format(stringDate).trim();
  }
}

module.exports = DatesBotHelper;
