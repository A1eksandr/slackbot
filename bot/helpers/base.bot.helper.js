const Globalize = require('../../config/globalize');
const globalize = Symbol('globalize');
const slack = Symbol('slack');

class BaseBotHelper {
  constructor(slackInstance) {
    this[slack] = slackInstance;
    this[globalize] = Globalize('ru');
  }

  get slack() {
    return this[slack];
  }

  get I18n(){
    return this[globalize];
  }
}

module.exports = BaseBotHelper;
