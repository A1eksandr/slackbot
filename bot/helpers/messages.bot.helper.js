const BaseBotHelper = require('./base.bot.helper');
const SlackMessage = require('../models/message.bot.model');

class MessagesBotHelper extends BaseBotHelper {
  sendMessage(messageText, channel, callback) {
    super.slack.sendMessage(messageText, channel.id, () => {
      if (callback) {
        callback();
      }
    });
  }

  getMessage(message) {
    if (!message || !message.text) {
      return;
    }
    return SlackMessage.parseText(message.text);
  }

  getMessageArgs(message) {
    if (!message || !message.text) {
      return;
    }
    return SlackMessage.getArgs(message.text);
  }

  getMessageCommand(message) {
    const [command,] = this.getMessage(message);

    return command;
  }

  getMessageText(message) {
    if (!message || !message.text) {
      return;
    }
    return message.text.replace(/^\s*\S*\s*/, '');
  }
}

module.exports = MessagesBotHelper;
