const BaseBotController = require('./base.bot.controller');

class AuthenticationsBotController extends BaseBotController {
  static done(rtmStartData) {
    console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}, but not yet connected to a channel`);
  }
}

module.exports = AuthenticationsBotController;
