const Globalize = require('../../config/globalize');
const DatesBotHelper = require('../helpers/dates.bot.helper');
const MessagesBotHelper = require('../helpers/messages.bot.helper');
const Channel = require('../models/channel.bot.model');
const slack = Symbol('slack');
const datesBotHelper = Symbol('datesBotHelper');
const messagesBotHelper = Symbol('messagesBotHelper');
const Permission = require('../../server/models').Permission;
const globalize = Symbol('globalize');

class BaseBotController {
  constructor(slackInstance) {
    this[slack] = slackInstance;
    this[datesBotHelper] = new DatesBotHelper(this[slack]);
    this[messagesBotHelper] = new MessagesBotHelper(this[slack]);
    this[globalize] = Globalize('ru');
  }

  get slack() {
    return this[slack];
  }

  get datesBotHelper() {
    return this[datesBotHelper];
  }

  get messagesBotHelper() {
    return this[messagesBotHelper];
  }

  get I18n() {
    return this[globalize];
  }

  _sendMessage(messageText, channel, callback) {
    this[slack].sendMessage(messageText, channel.id, () => {
      if (callback) {
        callback();
      }
    });
  }

  _getMessageArgs(message) {
    if (!message || !message.text) {
      return;
    }
    return message.text.split(' ').slice(1);
  }

  _getMessageText(message) {
    if (!message || !message.text) {
      return;
    }
    return message.text.split(' ').slice(1).join(' ');
  }

  _checkAdminPermissions(user) {
    return new Promise((resolve, reject) => {
      if (!user || !user.is_admin) {
        throw new Error('Для исполнения команды требуются права администратора');
      }
      resolve('Доступ на исполнение команды разрешен');
    });
  }

  _checkPermissions(user, action) {
    return Permission
      .find({
        raw: true,
        where: {
          userId: user.id,
          action: action
        }
      })
    .then(permission => {
      if (!permission && !user.is_admin) {
        throw new Error('У вас нет прав на исполнение данной команды');
      }
      return permission;
    });
  }
}

module.exports = BaseBotController;
