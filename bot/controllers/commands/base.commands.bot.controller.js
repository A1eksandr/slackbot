const BaseBotController = require('../base.bot.controller');
const actions = Symbol('actions');

class BaseCommandsBotController extends BaseBotController {
  constructor(slackInstance, actionsMap) {
    super(slackInstance);
    this[actions] = actionsMap;
  }

  get actions() {
    return this[actions];
  }

  _addAction(action, description) {
    if (!action || this[actions].has(action)) {
      return;
    }
    this[actions].set(action, description);
  }

  _checkMessageFormat(message, minLength = 1) {
    return new Promise((resolve, reject) => {
      const [command, ...args] = this.messagesBotHelper.getMessage(message);
      if (!args || args.length < minLength) {
        throw new Error(`Неверный формат команды. См.: help ${command}`);
      }
      resolve(args);
    });
  }
}

module.exports = BaseCommandsBotController;
