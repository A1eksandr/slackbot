const Rx = require('rx');
const BaseCommandsBotController = require('./base.commands.bot.controller');
const Task = require('../../../server/models').Task;
const User = require('../../../server/models').User;
const SlackChannel = require('../../models/channel.bot.model');
const UsersBotAdapter = require('../../adapters/users.bot.adapter');
const TasksBotHelper = require('../../helpers/tasks.bot.helper');
const tasksBotHelper = Symbol('tasksBotHelper');
const statuses = new Map([
  ['created', 'Создана'],
  ['postponed', 'Отложена'],
  ['process', 'В работе'],
  ['rejected', 'Отклонена'],
  ['finished', 'Завершена']
]);

class TasksCommandsBotController extends BaseCommandsBotController {
  constructor(slackInstance, actionsMap) {
    super(slackInstance, actionsMap);

    this[tasksBotHelper] = new TasksBotHelper(this.slack, statuses);

    this._addAction('listTask', {
      syntax: '[...<статус>]',
      description: `Отображает список задач текущего пользователя c указанными статусами. Если статусы не указаны, то\
 возвращаются все незавершенные задачи.`
    });
    this._addAction('listTaskAll', {
      syntax: '[...<статус>]',
      description: `Отображает список задач с указанными статусами. Если статусы не указаны, то отображаются все\
 незавершенные задачи. *Требуются права администратора либо права исполнения команды.`
    })
    this._addAction('createTask', {
      syntax: '<описание>',
      description: 'Создаёт новую задачу с указанным описанием.'
    });
    this._addAction('updateTask', {
      syntax: '<идентификатор> <описание>',
      description: `Обновляет описание задачи текущего пользователя. Администраторы могут\
 обновлять задачи всех пользователей.`
    });
    this._addAction('statusTask', {
      syntax: '<статус> <идентификатор> [...<идентификатор>]',
      description: `Назначает задачам с указанными идентификаторами статус, переданный в параметре. Список статусов:\n\
${[...statuses].map(([name, description]) => name + ' - Задача ' + description.toLowerCase()).join('\n')}`
    });
    this._addAction('destroyTask', {
      syntax: '<идентификатор> [...<идентификатор>]',
      description: 'Удаляет задачи с указанными идентификаторами.'
    });
  }

  listTaskAll(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);

    this._checkPermissions(user, 'listTaskAll')
      .then(msg => {
        const statuses = this.messagesBotHelper.getMessageArgs(message);

        return Task.findAll({
          raw: true,
          include: [{ model: User, as: 'user' }],
          where: {
            status: statuses.length ? statuses : { $not: 'finished' }
          }
        });
      })
      .then(tasks => {
        if (!tasks.length) {
          throw new Error('Нет сохраненных задач.')
        }
        return Rx.Observable.fromArray(tasks)
          .groupBy(task => task.userId)
          .flatMap(group => group.reduce((acc, task) => [...acc, this[tasksBotHelper].toString(task)], []));
      })
      .then(observable => {
        observable.subscribe(messages => this.messagesBotHelper.sendMessage(messages.join('\n'), dmChannel));
      })
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  listTask(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);
    const statuses = this.messagesBotHelper.getMessageArgs(message);
    const params = { userId: user.id };

    params['status'] = statuses.length ? statuses : { $not: 'finished' };

    Task.findAll({
      raw: true,
      where: {
        userId: user.id,
        status: statuses.length ? statuses : { $not: 'finished' }
      }
    })
    .then(tasks => {
      if (!tasks.length) {
        throw new Error('У вас нет сохраненных задач.');
      }
      return tasks.map(task => this[tasksBotHelper].toString(task));
    })
    .then(messages => this.messagesBotHelper.sendMessage(messages.join('\n'), dmChannel))
    .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  createTask(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);

    this._checkMessageFormat(message)
      .then(args => {
        return UsersBotAdapter.getUser(user);
      })
      .then(([currentUser,]) => {
        return Task.create({ description: this.messagesBotHelper.getMessageText(message), userId: currentUser.id });
      })
      .then(task => {
        const { dataValues: { id, description } } = task;

        this.messagesBotHelper.sendMessage(`> Задача \`${id}: ${description}\` успешно добавлена`, dmChannel);
      })
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  updateTask(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);

    this._checkMessageFormat(message, 2)
      .then(([taskId, ...description]) => {
        const params = { id: taskId };

        if (!user.is_admin) {
          params['userId'] = user.id;
        }

        return Task.update({ description: description.join(' ') }, { raw: true, where: params });
      })
      .then(([taskCount]) =>
        this.messagesBotHelper.sendMessage(
          `> Описание обновлено: ${taskCount} ${this.I18n.messageFormatter('task')(taskCount)}`,
          dmChannel
        )
      )
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  statusTask(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);

    this._checkPermissions(user, 'statusTask')
      .then(msg => {
        return this._checkMessageFormat(message, 2);
      })
      .then(() => {
        const [status, ...taskIds] = this.messagesBotHelper.getMessageArgs(message);
        if (!statuses.has(status)) {
          throw new Error(`Указан неверный статус. См.: help ${this.messagesBotHelper.getMessageCommand(message)}`);
        }

        return Task.update({ status: status }, { raw: true, where: { id: taskIds } });
      })
      .then(([taskCount]) =>
        this.messagesBotHelper.sendMessage(
          `> Обновлен статус: ${taskCount} ${this.I18n.messageFormatter('task')(taskCount)}`,
          dmChannel
        )
      )
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  destroyTask(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);

    this._checkMessageFormat(message)
      .then(taskIds => Task.findAll({ where: { id: taskIds, userId: user.id } }))
      .then(tasks => {
        if (!tasks.length) {
          throw new Error('Не найдены задачи с указанными идентификаторами.')
        }
        return Promise.all(tasks.map(task => task.destroy()));
      })
      .then(() => this.messagesBotHelper.sendMessage('\`Задачи с указанными удентификаторами удалены.\`', dmChannel))
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }
}

module.exports = TasksCommandsBotController;
