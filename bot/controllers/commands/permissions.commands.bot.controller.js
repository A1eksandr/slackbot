const Rx = require('rx');
const BaseCommandsBotController = require('./base.commands.bot.controller');
const User = require('../../../server/models').User;
const Permission = require('../../../server/models').Permission;
const SlackUser = require('../../models/user.bot.model');
const SlackChannel = require('../../models/channel.bot.model');
const UsersBotAdapter = require('../../adapters/users.bot.adapter');
const PermissionsBotHelper = require('../../helpers/permissions.bot.helper');
const associations = Symbol('associations');
const reversedAssociations = Symbol('reversedAssociations');
const permissionsBotHelper = Symbol('permissionsBotHelper');

class PermissionsCommandsController extends BaseCommandsBotController {
  constructor(slackInstance, actionsMap, associationsMap) {
    super(slackInstance, actionsMap);

    this[associations] = associationsMap;
    this[reversedAssociations] = new Map([...this[associations]].map(association => association.reverse()));
    this[permissionsBotHelper] = new PermissionsBotHelper(slackInstance, this[reversedAssociations]);

    this._addAction('createPermission', {
      syntax: '<команда> <пользователь> [...<пользователь>]',
      description: 'Предоставляет пользователям доступ к исполнению приватной команды бота. *Требуются права администратора.'
    });
    this._addAction('destroyPermission', {
      syntax: '<команда> <пользователь> [...<пользователь>]',
      description: 'Запрещает пользователям доступ к исполнению приватной команды бота. *Требуются права администратора.'
    });
    this._addAction('listPermission', {
      syntax: '[...<пользователь>]',
      description: 'Отображает список доступов пользователя к исполнению приватных команд бота. Администраторы видят полный список.'
    });
  }

  listPermission(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);
    const params = {};

    if (!user.is_admin) {
      params['userId'] = user.id;
    }

    Permission
      .findAll({
        raw: true,
        include: [
          { model: User, as: 'user' },
          { model: User, as: 'granter' }
        ],
        where: params
      })
      .then(permissions => {
        if (!permissions.length) {
          throw new Error('Предоставленных доступов не найдено');
        }

        return Rx.Observable.fromArray(permissions)
          .groupBy(permission => permission.action)
          .flatMap(group => group.reduce((acc, permission) => [
            ...acc,
            this[permissionsBotHelper].toString(permission)
          ], []));
      })
      .then(observable => {
        observable.subscribe(group => this.messagesBotHelper.sendMessage(group.join('\n'), dmChannel));
      })
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  createPermission(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);
    const [command, ...userIds] = this.messagesBotHelper.getMessageArgs(message);

    this._checkAdminPermissions(user)
      .then(msg => this._changePermission('createPermission', user, userIds))
      .then(results => {
        this._checkCommandPresence(command);

        const [[admin, adminCreated], ...users] = results;

        return Promise.all(
          users.map(([currentUser, userCreated]) =>
            Permission.findOrCreate({
              raw: true,
              include: [
                { model: User, as: 'user' },
                { model: User, as: 'granter' }
              ],
              where: {
                userId: currentUser.id,
                granterId: admin.id,
                action: this[associations].get(command)
              }
            })
          )
        );
      })
      .then(results =>
        results.map(([permission, permissionCreated]) => {
          if (permissionCreated) {
            const { dataValues: { userId } } = permission;
            const [permissionUser] = SlackUser.findByIdOrName(this.slack, [userId]);
            const userDMChannel = SlackChannel.getDM(this.slack, permissionUser);

            if (userDMChannel) {
              this.messagesBotHelper.sendMessage(
                `Вам были предоставлены права на исполнение команды \`${this[reversedAssociations].get(permission.action)}\``,
                userDMChannel
              );
            }

            return `> Пользователю *${permissionUser.name}* предоставлены права на исполнение команды\
 \`${this[reversedAssociations].get(permission.action)}\``;
          }
          return `> Пользователю *${permission['user.name']}* ранее уже были предоставлены пользователем\
 *${permission['granter.name']}* права на исполнение команды \`${this[reversedAssociations].get(permission.action)}\``;
        })
      )
      .then(messages => this.messagesBotHelper.sendMessage(messages.join('\n'), dmChannel))
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  destroyPermission(message, channel, user) {
    const dmChannel = SlackChannel.getDM(this.slack, user);
    const [command, ...userIds] = this.messagesBotHelper.getMessageArgs(message);

    this._checkAdminPermissions(user)
      .then(msg => this._changePermission('destroyPermission', user, userIds))
      .then(results => {
        this._checkCommandPresence(command);

        const [[admin, adminCreated], ...users] = results;

        return users.map(([currentUser, userCreated]) => currentUser.id);
      })
      .then(userIds =>
        Permission.findAll({
          where: {
            userId: userIds,
            action: this[associations].get(command)
          }
        })
      )
      .then(permissions => Promise.all(permissions.map(permission => permission.destroy())))
      .then(() => {
        const slackUsersNames = SlackUser.findByIdOrName(this.slack, userIds).map(slackUser => `*${slackUser.name}*`);
        this.messagesBotHelper.sendMessage(`У пользователей ${slackUsersNames.join(', ')} отозваны права на исполнение\
 команды \`${command}\``, dmChannel);
      })
      .catch(error => this.messagesBotHelper.sendMessage(`> \`${error.message}\``, dmChannel));
  }

  _changePermission(methodName, user, userIds) {
    if (!userIds || userIds.length < 1) {
      throw new Error(`Неверный формат команды. См.: help ${this[reversedAssociations].get(methodName)}`);
    }
    return UsersBotAdapter.getUsers(user, ...SlackUser.findByIdOrName(this.slack, userIds));
  }

  _checkCommandPresence(command) {
    if (!this[associations].has(command)) {
      throw new Error(`Команда ${command} не найдена среди доступных команд. См.: help`);
    }
  }
}

module.exports = PermissionsCommandsController;
