const _ = require('lodash');
const Channel = require('../../models/channel.bot.model');
const BaseCommandsBotController = require('./base.commands.bot.controller');
const associations = Symbol('associations');

class HelpsCommandsBotController extends BaseCommandsBotController {
  constructor(slackInstance, actionsMap, associationsMap) {
    super(slackInstance, actionsMap);
    super._addAction('help', {
      syntax: '[...<команда>]',
      description: `Отображает справку по выбранной команде бота.\nСписок команд: ${Array.from(associationsMap.keys()).join(', ')}.`
    });
    this[associations] = associationsMap;
  }

  help(message, channel, user) {
    const dmChannel = Channel.getDM(this.slack, user);
    const commands = this.messagesBotHelper.getMessageArgs(message);

    new Promise((resolve, reject) => {
      let messages = _
        .chain(commands)
        .uniq()
        .filter(command => command.trim().length)
        .map(command => this._formatDescription(command))
        .value();
      if (!messages.length) {
        reject(this._formatDescription('help'));
      }
      resolve(messages.join('\n'));
    })
    .then(messageText => this.messagesBotHelper.sendMessage(`\`\`\`${messageText}\`\`\``, dmChannel))
    .catch(messageText => this.messagesBotHelper.sendMessage(`\`\`\`${messageText}\`\`\``, dmChannel));
  }

  _formatDescription(command) {
    let action = this.actions.get(this[associations].get(command));
    return action ? `${command} ${action.syntax} - ${action.description}` : `${command} - Команда не найдена`;
  }
}

module.exports = HelpsCommandsBotController;
