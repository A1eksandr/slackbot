const BaseBotController = require('./base.bot.controller');
const User = require('../models/user.bot.model');
const Team = require('../models/team.bot.model');

class ConnectionsBotController extends BaseBotController {
  opened() {
    let user = User.find(this.slack);
    let team = Team.find(this.slack);

    console.log(`Connected to ${team.name} as ${user.name}`);
  }
}

module.exports = ConnectionsBotController;
