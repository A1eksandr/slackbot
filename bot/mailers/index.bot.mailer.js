const transporter = require('../../config/transporter');
const _ = require('lodash');
const config = require('../../config/config');

class IndexMailer {
  static send(options, callback) {
    const mailOptions = _.merge(config.mailer, options);

    transporter.sendMail(mailOptions, (error, info) => {
      console.log('Mail sended');
      if (callback) {
        callback(error, info);
      }
    });
  }
}

module.exports = IndexMailer;

