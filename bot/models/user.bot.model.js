const _ = require('lodash');

class User {
  static find(slack, userId = slack.activeUserId) {
    return slack.dataStore.getUserById(this.clearUserId(userId));
  }

  static findByName(slack, userName) {
    return slack.dataStore.getUserByName(this.clearUserId(userName));
  }

  static findByIdOrName(slack, ...userIds) {
    return _.chain(...userIds)
            .map(userId => { return this.find(slack, userId) || this.findByName(slack, userId); })
            .filter(slackUser => slackUser)
            .value();
  }

  static clearUserId(userId) {
    return userId.replace(/[<>@]/gi, '');
  }
}

module.exports = User;
