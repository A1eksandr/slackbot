const _ = require('lodash');
const deasync = require('deasync');
const WebClient = require('@slack/client').WebClient;

class Channel {
  static all(slack) {
    return slack.dataStore.channels;
  }

  static find(slack, channelId) {
    return all(slack)[channelId];
  }

  static current(slack) {
    return _.filter(all(slack), channel => { return channel.is_member });
  }

  static getDM(slack, user) {
    let dmChannel = slack.dataStore.getDMByName(user.name);

    if (!dmChannel) {
      const webClient = new WebClient(slack['_token']);

      webClient.im.open(user.id, (error, result) => {
        if (error) {
          throw new Error(error);
        }
        dmChannel = result.channel;
      });
      while(dmChannel === undefined) { deasync.sleep(100); }
    }
    return dmChannel;
  }
}

module.exports = Channel;
