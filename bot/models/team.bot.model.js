class Team {
  static find(slack, teamId = slack.activeTeamId) {
    return slack.dataStore.getTeamById(teamId);
  }
}

module.exports = Team;
