const User = require('./user.bot.model');

class Message {
  constructor(slack) {
    this.slack = slack;
  }

  static send(slack, message, channel, callback) {
    slack.sendMessage(message, channel.id, () => {
      if (callback) {
        callback();
      }
    });
  }

  static parseText(text) {
    return text.trim().replace(/\s{2,}/g, ' ').split(' ');
  }

  static getArgs(text) {
    return this.parseText(text).slice(1);
  }

  static getText(text) {
    return this.getArgs(text).join(' ');
  }
}

module.exports = Message;
