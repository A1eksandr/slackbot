const BaseBotEvent = require('./base.bot.events');
const User = require('../models/user.bot.model');
const PermissionsCommandsBotController = require('../controllers/commands/permissions.commands.bot.controller');
const TasksCommandsBotController = require('../controllers/commands/tasks.commands.bot.controller');
const HelpsCommandsBotController = require('../controllers/commands/helps.commands.bot.controller');
const CommandListener = require('../listeners/command.bot.listener');

const ASSOCIATIONS = new Map([
  ['help', 'help'],
  ['access_add', 'createPermission'],
  ['access_remove', 'destroyPermission'],
  ['access_list', 'listPermission'],
  ['task_list', 'listTask'],
  ['task_all', 'listTaskAll'],
  ['task_add', 'createTask'],
  ['task_update', 'updateTask'],
  ['task_status', 'statusTask'],
  ['task_remove', 'destroyTask']
]);
const ACTIONS = new Map();

class CommandEvent extends BaseBotEvent {
  constructor(slack) {
    super(slack);

    this.helpsCommandsBotController = new HelpsCommandsBotController(this.slack, ACTIONS, ASSOCIATIONS);
    this.permissionsCommandsBotController = new PermissionsCommandsBotController(this.slack, ACTIONS, ASSOCIATIONS);
    this.tasksCommandsBotController = new TasksCommandsBotController(this.slack, ACTIONS);
    this.commandListener = new CommandListener(this.slack);

    // Help
    this.commandListener.on('help', (message, channel, user) => {
      this.helpsCommandsBotController.help(message, channel, user);
    }, true);

    // Permissions
    this.commandListener.on('access_add', (message, channel, user) => {
      this.permissionsCommandsBotController.createPermission(message, channel, user);
    }, true);
    this.commandListener.on('access_remove', (message, channel, user) => {
      this.permissionsCommandsBotController.destroyPermission(message, channel, user);
    }, true);
    this.commandListener.on('access_list', (message, channel, user) => {
      this.permissionsCommandsBotController.listPermission(message, channel, user);
    }, true);

    // Tasks
    this.commandListener.on('task_list', (message, channel, user) => {
      this.tasksCommandsBotController.listTask(message, channel, user);
    }, true);
    this.commandListener.on('task_all', (message, channel, user) => {
      this.tasksCommandsBotController.listTaskAll(message, channel, user);
    }, true);
    this.commandListener.on('task_add', (message, channel, user) => {
      this.tasksCommandsBotController.createTask(message, channel, user);
    }, true);
    this.commandListener.on('task_update', (message, channel, user) => {
      this.tasksCommandsBotController.updateTask(message, channel, user);
    }, true);
    this.commandListener.on('task_status', (message, channel, user) => {
      this.tasksCommandsBotController.statusTask(message, channel, user);
    }, true);
    this.commandListener.on('task_remove', (message, channel, user) => {
      this.tasksCommandsBotController.destroyTask(message, channel, user);
    }, true);
  }

  send(message) {
    this.commandListener.send(message);
  }
}

module.exports = CommandEvent;
