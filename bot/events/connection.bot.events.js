'use strict';

const CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
const ConnectionsBotController = require('../controllers/connections.bot.controller');

class ConnectionEvent {
  constructor(slack) {
    this.slack = slack;
    this.connectionsBotController = new ConnectionsBotController(this.slack);
    this.slack.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, () => this.connectionsBotController.opened(this.slack));
  }
}

module.exports = ConnectionEvent;
