'use strict';

const RTM_EVENTS = require('@slack/client').RTM_EVENTS;
const CommandEvent = require('./command.bot.events');

class MessageEvent {
  constructor(slack) {
    this.slack = slack;
    this.commandEvent = new CommandEvent(this.slack);
    this.slack.on(RTM_EVENTS.MESSAGE, (message) => this.commandEvent.send(message));
  }
}

module.exports = MessageEvent;
