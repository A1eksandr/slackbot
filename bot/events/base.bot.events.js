const slack = Symbol('slack');

class BaseBotEvent {
  constructor(slackInstance) {
    this[slack] = slackInstance;
  }

  get slack() {
    return this[slack];
  }
}

module.exports = BaseBotEvent;
