'use strict';

const CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
const AuthenticationsController = require('../controllers/authentications.bot.controller');

class AuthenticationEvent {
  constructor(slack) {
    this.slack = slack;
    this.slack.on(CLIENT_EVENTS.RTM.AUTHENTICATED, (rtmStartData) => AuthenticationsController.done(rtmStartData));
  }
}

module.exports = AuthenticationEvent;

