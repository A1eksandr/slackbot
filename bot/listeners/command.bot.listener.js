'use strict';

const User = require('../models/user.bot.model');

class CommandListner {
  constructor(slack) {
    this.slack = slack;
    this.keywords = new Map();
  }

  send(message) {
    if (!message.text) {
      return false;
    }

    let channel = this.slack.dataStore.getChannelGroupOrDMById(message.channel);
    let user = User.find(this.slack, message.user);
    for (let regex of this.keywords.keys()) {
      if (regex.test(message.text)) {
        let callback = this.keywords.get(regex);
        callback(message, channel, user);
      }
    }
  }

  on(keywords, callback, start) {
    if (start) {
      keywords = `^${keywords}((\\s+.*)|$)`;
    }
    let regex = new RegExp(keywords, 'i');
    this.keywords.set(regex, callback);
  }
}

module.exports = CommandListner;
