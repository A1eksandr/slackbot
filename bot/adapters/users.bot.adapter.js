const User = require('../../server/models').User;

class UsersBotAdapter {
  static getUser(slackUser) {
    return User.findOrCreate({
      raw: true,
      where: { id: slackUser.id },
      defaults: {
        name: slackUser.name,
        firstName: slackUser.profile['first_name'],
        lastName: slackUser.profile['last_name']
      }
    });
  }

  static getUsers(...slackUsers) {
    return Promise.all(slackUsers.map(slackUser => this.getUser(slackUser)));
  }
}

module.exports = UsersBotAdapter;
