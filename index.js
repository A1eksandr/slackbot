process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const config = require('./config/config');
const Bot = require('./config/bot');
const bot = new Bot({
  slackToken: config.slackToken,
  autoReconnect: config.bot.autoReconnect,
  autoMark: config.bot.autoMark
});

bot.start();
