module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
    description: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    status: {
      allowNull: false,
      type: DataTypes.ENUM,
      values: ['created', 'postponed', 'process', 'rejected', 'finished'],
      defaultValue: 'created'
    }
  }, {
    tableName: 'tasks',
    classMethods: {
      associate: (models) => {
        Task.belongsTo(models.User, {
          foreignKey: 'userId',
          onDelete: 'CASCADE',
          as: 'user'
        });
      },
    },
  });
  return Task;
};
