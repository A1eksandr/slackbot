module.exports = (sequelize, DataTypes) => {
  const Permission = sequelize.define('Permission', {
    action: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    tableName: 'permissions',
    classMethods: {
      associate: function(models) {
        Permission.belongsTo(models.User, {
          foreignKey: 'userId',
          onDelete: 'CASCADE',
          as: 'user',
        });
        Permission.belongsTo(models.User, {
          foreignKey: 'granterId',
          onDelete: 'CASCADE',
          as: 'granter',
        });
      },
    },
  });
  return Permission;
};
