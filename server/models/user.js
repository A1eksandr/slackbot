module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      allowNull: false,
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    firstName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.STRING,
    },
  }, {
    tableName: 'users',
    classMethods: {
      associate: (models) => {
        User.hasMany(models.Task, {
          foreignKey: 'userId',
          as: 'tasks',
        });
        User.hasMany(models.Permission, {
          foreignKey: 'userId',
          as: 'permissions',
        });
        User.hasMany(models.Permission, {
          foreignKey: 'granterId',
          as: 'grantedPermissions',
        });
      },
    },
  });
  return User;
};
