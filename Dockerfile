FROM node:boron
MAINTAINER Aleksandr Kiselev <aleksandr.developer@gmail.com>

# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:
ADD package.json /tmp/package.json
RUN npm i -g sequelize-cli@2.5.1 nodemon@1.11.0
RUN cd /tmp && npm install
RUN mkdir -p /usr/src/app && cp -a /tmp/node_modules /usr/src/app/

# Install dependencies
WORKDIR /usr/src/app
ADD . /usr/src/app
