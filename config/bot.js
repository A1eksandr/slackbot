const slackClient = require('@slack/client');
const RtmClient = slackClient.RtmClient;
const MemoryDataStore = slackClient.MemoryDataStore;
const CLIENT_EVENTS = slackClient.CLIENT_EVENTS;
const RTM_EVENTS = slackClient.RTM_EVENTS;
const AuthenticationEvent = require('../bot/events/authentication.bot.events');
const ConnectionEvent = require('../bot/events/connection.bot.events');
const MessageEvent = require('../bot/events/message.bot.events');

class Bot {
  constructor(opts) {
    let slackToken = opts.slackToken;
    let autoReconnect = opts.autoReconnect || true;
    let autoMark = opts.autoMark || true;

    this.slack = new RtmClient(slackToken, {
      LogLevel: 'error',
      dataStore: new MemoryDataStore(),
      autoReconnect: autoReconnect,
      autoMark: autoMark
    });

    this.authenticationEvent = new AuthenticationEvent(this.slack);
    this.connectionEvent = new ConnectionEvent(this.slack);
    this.messageEvent = new MessageEvent(this.slack);
  }

  start() {
    this.slack.start();
  }
}

module.exports = Bot;
