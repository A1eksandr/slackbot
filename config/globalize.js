const Globalize = require('globalize');
const CLDRData = require('cldr-data');
const config = require('./config');
const messages = require('./globalize/messages');
const locales = config.locales || ['ru'];

Globalize.load(CLDRData.entireSupplemental());
Globalize.load(CLDRData.entireMainFor(...locales));
Globalize.loadMessages(messages);

module.exports = Globalize;
