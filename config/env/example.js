'use strict';

module.exports = {
  slackToken: '',
  bot: {
    LogLevel: 'error',
    autoReconnect: true,
    autoMark: true
  }
}

